import { createWebHistory, createRouter } from "vue-router";
import LoginPage from "@/components/login/LoginPage.vue";
import DashboardCabang from "@/components/Cabang/DashboardCabang.vue";
import CabangPageHarian from "@/components/Cabang/CabangPageHarian.vue";
import CabangPageBulanan from "@/components/Cabang/CabangPageBulanan.vue";
import CabangUtamaPage from "@/components/Cabang/CabangUtamaPage.vue";
import CabangUtamaBulananPage from "@/components/Cabang/CabangUtamaBulananPage.vue";
import TopCabang from "@/components/Cabang/TopCabang.vue";
import TopUtama from "@/components/Cabang/TopUtama.vue";
import TambahAkunCabang from "@/components/Cabang/TambahAkunCabang.vue";
import ProdukKosong from "@/components/Cabang/ProdukKosong.vue";
import ProdukKosongCabang from "@/components/Cabang/ProdukKosongCabang.vue";
import MainLayout from "@/components/template/MainLayout";
import MainLayoutUtama from "@/components/template/MainLayoutUtama";
import JumlahTransaksiCabang from "@/components/Cabang/JumlahTransaksiCabang.vue";
import JumlahTransaksi from "@/components/Cabang/JumlahTransaksi.vue";
import HistoryProduk from "@/components/Cabang/HistoryProduk.vue";
import HistoryProdukCabang from "@/components/Cabang/HistoryProdukCabang.vue";
import AnalisaProdukCabang from "@/components/AnalisaProduk/AnalisaProdukCabang.vue";
import AnalisaProduk from "@/components/AnalisaProduk/AnalisaProduk.vue";
import AnalisaProdukVs from "@/components/AnalisaProduk/AnalisaProdukVs.vue";
import InsentifTindakan from "@/components/Insentif/InsentifTindakan.vue";
import InsentifTindakanCabang from "@/components/Insentif/InsentifTindakanCabang.vue";
import InsentifTindakanAll from "@/components/Insentif/InsentifTindakanAll.vue";
import InsentifTindakanAllCabang from "@/components/Insentif/InsentifTindakanAllCabang.vue";
import InsentifDokter from "@/components/Insentif/InsentifDokter.vue";
import InsentifDokterCabang from "@/components/Insentif/InsentifDokterCabang.vue";
import InsentifApoteker from "@/components/Insentif/InsentifApoteker.vue";
import InsentifApotekerCabang from "@/components/Insentif/InsentifApotekerCabang.vue";
import InsentifCabang from "@/components/Insentif/InsentifCabang.vue";
import InsentifJabatanDokter from "@/components/Insentif/InsentifJabatanDokter.vue";
import StokProduk from "@/components/Cabang/StokProduk.vue";
import StokProdukCabang from "@/components/Cabang/StokProdukCabang.vue";
import PenjualanVoucher from "@/components/Voucher/PenjualanVoucher.vue";
import PemakaianVoucher from "@/components/Voucher/PemakaianVoucher.vue";
import TestPageTalenta from "@/servis/TestPageTalenta"

const routes = [
  { path: "/", name: "LoginPage", component: LoginPage },
  {
    path: "/main",
    name: "MainLayout",
    component: MainLayout,
    children: [
      {
        path: "/cabangharian",
        name: "CabangPageHarian",
        component: CabangPageHarian,
      },
      {
        path: "/cabangbulanan",
        name: "CabangPageBulanan",
        component: CabangPageBulanan,
      },
      {
        path: "/topcabang",
        name: "TopCabang",
        component: TopCabang,
      },
      {
        path: "/produkkosongcabang",
        name: "ProdukKosongCabang",
        component: ProdukKosongCabang,
      },
      {
        path: "/jumlahtransaksicabang",
        name: "JumlahTransaksiCabang",
        component: JumlahTransaksiCabang,
      },
      {
        path: "/analisaprodukcabang",
        name: "AnalisaProdukcabang",
        component: AnalisaProdukCabang,
      },
      {
        path: "/insentiftindakancabang",
        name: "InsentifTindakanCabang",
        component: InsentifTindakanCabang,
      },
      {
        path: "/insentiftindakanallcabang",
        name: "InsentifTindakanAllCabang",
        component: InsentifTindakanAllCabang,
      },
      {
        path: "/insentifdoktercabang",
        name: "InsentifDokterCabang",
        component: InsentifDokterCabang,
      },
      {
        path: "/insentifapotekercabang",
        name: "InsentifApotekerCabang",
        component: InsentifApotekerCabang,
      },
      {
        path: "/stokprodukcabang",
        name: "StokProdukCabang",
        component: StokProdukCabang,
      },
     
      {
        path: "/produkkosongcabang",
        name: "ProdukKosongCabang",
        component: ProdukKosongCabang,
      },
      {
        path: "/historyprodukcabang",
        name: "HistoryProdukCabang",
        component: HistoryProdukCabang,
      },
     
     
     
    ],
  },
  {
    path: "/mainutama",
    name: "MainLayoutUtama",
    component: MainLayoutUtama,
    children: [
      {
        path: "/cabangutama",
        name: "CabangUtamaPage",
        component: CabangUtamaPage,
      },
      {
        path: "/cabangutamabulanan",
        name: "CabangUtamaBulananPage",
        component: CabangUtamaBulananPage,
      },
      {
        path: "/toputama",
        name: "TopUtama",
        component: TopUtama,
      },
      {
        path: "/tambahcabang",
        name: "TambahAkunCabang",
        component: TambahAkunCabang,
      },
      {
        path: "/produkkosong",
        name: "ProdukKosong",
        component: ProdukKosong,
      },
      {
        path: "/jumlahtransaksi",
        name: "JumlahTransaksi",
        component: JumlahTransaksi,
      },
      {
        path: "/analisaproduk",
        name: "AnalisaProduk",
        component: AnalisaProduk,
      },
      {
        path: "/AnalisaProdukVs",
        name: "AnalisaProdukVs",
        component: AnalisaProdukVs,
      },
      {
        path: "/insentiftindakan",
        name: "InsentifTindakan",
        component: InsentifTindakan,
      },
      {
        path: "/insentiftindakanall",
        name: "InsentifTindakanAll",
        component: InsentifTindakanAll,
      },
      {
        path: "/insentifdokter",
        name: "InsentifDokter",
        component: InsentifDokter,
      },
      {
        path: "/insentifapoteker",
        name: "InsentifApoteker",
        component: InsentifApoteker,
      },
      {
        path: "/insentifcabang",
        name: "InsentifCabang",
        component: InsentifCabang,
      },
      {
        path: "/insentifjabatandokter",
        name: "InsentifJabatanDokter",
        component: InsentifJabatanDokter,
      },
      {
        path: "/stokproduk",
        name: "StokProduk",
        component: StokProduk,
      },
      {
        path: "/historyproduk",
        name: "HistoryProduk",
        component: HistoryProduk,
      },
      {
        path: "/dashcabang",
        name: "DashboardCabang",
        component: DashboardCabang,
      },
      {
        path: "/testtalenta",
        name: "TestPageTalenta",
        component: TestPageTalenta,
      },
      {
        path: "/penjualanvoucher",
        name: "PenjualanVoucher",
        component: PenjualanVoucher,
      },
      {
        path: "/pemakaianvoucher",
        name: "PemakaianVoucher",
        component: PemakaianVoucher,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  linkActiveClass: "selected",
});

export default router;
