import { defineStore } from "pinia";
import axios from "axios";
import cookie from "vue-cookies";

import {
  dataJumlahTransaksi,
  dataReal,
  dataTgl,
  dateFormat,
  formattedDate,
  formatRupiah,
} from "./functions";
import md5 from "md5";
import CryptoJS from "crypto-js";
import url from "url";

// import md5 from "md5";

export const useTaskStore = defineStore("counter", {
  state: () => ({
    //get token and id
    API: process.env.VUE_APP_API_URL,
    Token: cookie?.get("token"),
    // token2:"",
    Token2: cookie?.get("user")?.token,

    bulanSelect: [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "July",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ],
    arrayOfDates: [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
      22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    ],
    //login state
    Password: "",
    Username: "",
    passworderr: "",

    // add akun cabang state
    noInduk: "",
    jabatan: "",
    Upassowrd: "",
    Uname: "",
    NamaLengkap: "",

    //chartjs
    bulan: "",
    tahun: "",
    cabang: "",
    gudang: "",
    bulanAwal: "",
    bulanAkhir: "",
    listCabang: [],
    listCabangall: [],
    listjabatan: [],
    listGudang: [],
    topjasa: [],
    topproduk: [],
    topapotek: [],
    topresep: [],
    stokKosong: [],
    IdInventory: "",
    chartData: {},
    // data: {},
    data: {
      // labels: ,
      datasets: [
        {
          label: "Apotek",
          backgroundColor: "#D1AD9F",
          borderColor: "#D1AD9F",
        },
        {
          label: "Jasa",
          backgroundColor: "#B74A1F",
          borderColor: "#B74A1F",
        },
        {
          label: "Produk",
          backgroundColor: "#5C69A3",
          borderColor: "#5C69A3",
        },
        {
          label: "Resep",
          backgroundColor: "#555C05",
          borderColor: "#555C05",
        },
      ],
    },
    dataSemua: {
      // labels: ,
      datasets: [
        {
          label: "Semua",
          backgroundColor: "#5F5F5F",
          borderColor: "#5F5F5F",
        },
      ],
    },

    dataCabang: {
      datasets: [
        {
          label: "Apotek",
          backgroundColor: "#D1AD9F",
          borderColor: "#D1AD9F",
        },
        {
          label: "Jasa",
          backgroundColor: "#B74A1F",
          borderColor: "#B74A1F",
        },
        {
          label: "Produk",
          backgroundColor: "#5C69A3",
          borderColor: "#5C69A3",
        },
        {
          label: "Resep",
          backgroundColor: "#555C05",
          borderColor: "#555C05",
        },
      ],
    },

    dataSemuaCabang: {
      // labels: ,
      datasets: [
        {
          label: "Semua",
          backgroundColor: "#5F5F5F",
          borderColor: "#5F5F5F",
        },
      ],
    },

    dataJumlahTransaksi: {
      datasets: [
        {
          label: "Apotek",
          backgroundColor: "#D1AD9F",
          borderColor: "#D1AD9F",
        },
        {
          label: "Jasa",
          backgroundColor: "#B74A1F",
          borderColor: "#B74A1F",
        },
        {
          label: "Produk",
          backgroundColor: "#5C69A3",
          borderColor: "#5C69A3",
        },
        {
          label: "Resep",
          backgroundColor: "#555C05",
          borderColor: "#555C05",
        },
      ],
    },
    dataJumlahSemuaTransaksi: {
      // labels: ,
      datasets: [
        {
          label: "Semua",
          backgroundColor: "#5F5F5F",
          borderColor: "#5F5F5F",
        },
      ],
    },
    tipe: "",

    succes: false,
    isLoading: false,
    cobac: [],
    cobacvs: [],
    totalprice: [],
    Sum: "",
    SumAnalisa: "",
    totalpricevs: [],
    SumVS: "",
    SumAnalisaVs: "",
    jenisTindakan: "",
    DataTindakan: [],
    cabangvs: "",
    tipevs: "",
    bulanvs: "",
    tahunvs: "",
    ListApoteker: [],
    tglAwal: "",
    tglAkhir: "",
    ProdukDokter: [],
    ProdukTdkDitebus: [],
    ProdukmergedArray: [],
    HttdDokter: [],
    ListInventory: [],
    StokProduk: [],
    LogInventory: [],
    ListOwner: [],
    UsernameOwner: "",
    PasswordOwner: "",
    PasswordOwnerCon: "",

    //voucher
    penjualanVoucher: [],
    pemakaianVoucher: [],

    Errormsg: "",
  }),
  getters: {},
  actions: {
    emptyAllState() {
      (this.bulan = ""),
        (this.tahun = ""),
        (this.cabang = ""),
        (this.bulanAwal = ""),
        (this.bulanAkhir = ""),
        (this.bulanAkhir = ""),
        (this.jabatan = ""),
        (this.NamaLengkap = ""),
        (this.Uname = ""),
        (this.Upassowrd = ""),
        (this.noInduk = ""),
        (this.gudang = "");
      this.tipe = "";
      this.stokKosong = [];
    },

    getListCabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };

      axios
        .get(`${this.API}//api/1.0/workflow/extrarest/v1/s/listCabang2`, config)
        .then((res) => {
          if (res.data.response) {
            this.listCabang = res.data.response;
            this.listCabang.push({ id: "all", text: "Semua" });
          } else {
            this.isLoading = false;
          }
        });
    },

    getListCabangAll() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };

      axios
        .get(`${this.API}//api/1.0/workflow/extrarest/v1/s/listCabang2`, config)
        .then((res) => {
          if (res.data.response) {
            this.listCabangall = res.data.response;
            this.listCabangall.push({ id: "all", text: "All Cabang" });
          } else {
            this.isLoading = false;
          }
        });
    },

    getListJabatan() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };

      axios
        .get(`${this.API}//api/1.0/workflow/extrarest/v1/s/listJabatan`, config)
        .then((res) => {
          if (res.data.response) {
            this.listjabatan = res.data.response;
          } else {
            this.isLoading = false;
          }
        });
    },

    async getPendapatanUtama() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;

      try {
        await axios
          .get(
            `${this.API}//api/1.0/workflow/extrarest/v1/s/pendapatanHarianCabang/${this.cabang}/${this.bulan}/${this.tahun}`,
            config
          )
          .then((res) => {
            if (res.data.response[0]) {
              this.data = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Apotek",
                    backgroundColor: "#D1AD9F",
                    data: dataTgl(
                      res.data.response[0].apotek?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#D1AD9F",
                  },
                  {
                    label: "Jasa",
                    backgroundColor: "#B74A1F",
                    data: dataTgl(res.data.response[0].jasa?.map((d) => d)).map(
                      (d) => d
                    ),
                    borderColor: "#B74A1F",
                  },
                  {
                    label: "Produk",
                    backgroundColor: "#5C69A3",
                    data: dataTgl(
                      res.data.response[0].produk?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5C69A3",
                  },
                  {
                    label: "Resep",
                    backgroundColor: "#555C05",
                    data: dataTgl(
                      res.data.response[0].resep?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#555C05",
                  },
                ],
              };
              this.dataSemua = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Semua",
                    backgroundColor: "#5F5F5F",
                    data: dataTgl(
                      res.data.response[0].semua?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5F5F5F",
                  },
                ],
              };
              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          });
      } catch (error) {
        console.log(error);
      }
    },

    getPendapatanUtamaBulanan() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          `${this.API}//api/1.0/workflow/extrarest/v1/s/pendapatanBulananCabang/${this.cabang}/1/12/${this.tahun}`,
          config
        )
        .then((res) => {
          if (res.data.response[0]) {
            this.data = {
              labels: this.bulanSelect,
              datasets: [
                {
                  label: "Apotek",
                  backgroundColor: "#D1AD9F",
                  data: dataReal(
                    res.data.response[0].apotek?.map((d) => d)
                  ).map((d) => d),
                  borderColor: "#D1AD9F",
                },
                {
                  label: "Jasa",
                  backgroundColor: "#B74A1F",
                  data: dataReal(res.data.response[0].jasa?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#B74A1F",
                },
                {
                  label: "Produk",
                  backgroundColor: "#5C69A3",
                  data: dataReal(
                    res.data.response[0].produk?.map((d) => d)
                  ).map((d) => d),
                  borderColor: "#5C69A3",
                },
                {
                  label: "Resep",
                  backgroundColor: "#555C05",
                  data: dataReal(res.data.response[0].resep?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#555C05",
                },
              ],
            };
            this.dataSemua = {
              labels: this.bulanSelect,
              datasets: [
                {
                  label: "Semua",
                  backgroundColor: "#565454",
                  data: dataReal(res.data.response[0].semua?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#565454",
                },
              ],
            };

            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },

    async getCabangData() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      try {
        await axios
          .get(
            `${
              this.API
            }//api/1.0/workflow/extrarest/v1/s/pendapatanHarianCabang/${
              cookie?.get("user")?.id_cabang
            }/${this.bulan}/${this.tahun}`,
            config
          )
          .then((res) => {
            console.log(res);
            if (res.data.response[0]) {
              this.dataCabang = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Apotek",
                    backgroundColor: "#D1AD9F",
                    data: dataTgl(
                      res.data.response[0].apotek?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#D1AD9F",
                  },
                  {
                    label: "Jasa",
                    backgroundColor: "#B74A1F",
                    data: dataTgl(res.data.response[0].jasa?.map((d) => d)).map(
                      (d) => d
                    ),
                    borderColor: "#B74A1F",
                  },
                  {
                    label: "Produk",
                    backgroundColor: "#5C69A3",
                    data: dataTgl(
                      res.data.response[0].produk?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5C69A3",
                  },
                  {
                    label: "Resep",
                    backgroundColor: "#555C05",
                    data: dataTgl(
                      res.data.response[0].resep?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#555C05",
                  },
                ],
              };
              this.dataSemuaCabang = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Semua",
                    backgroundColor: "#5F5F5F",
                    data: dataTgl(
                      res.data.response[0].semua?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5F5F5F",
                  },
                ],
              };
              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          });
      } catch (error) {
        console.log(error);
      }
    },

    getCabangBulanan() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          `${
            this.API
          }//api/1.0/workflow/extrarest/v1/s/pendapatanBulananCabang/${
            cookie?.get("user")?.id_cabang
          }/1/12/${this.tahun}`,
          config
        )
        .then((res) => {
          if (res.data.response[0]) {
            this.dataCabang = {
              labels: this.bulanSelect,
              datasets: [
                {
                  label: "Apotek",
                  backgroundColor: "#D1AD9F",
                  data: dataReal(
                    res.data.response[0].apotek?.map((d) => d)
                  ).map((d) => d),
                  borderColor: "#D1AD9F",
                },
                {
                  label: "Jasa",
                  backgroundColor: "#B74A1F",
                  data: dataReal(res.data.response[0].jasa?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#B74A1F",
                },
                {
                  label: "Produk",
                  backgroundColor: "#5C69A3",
                  data: dataReal(
                    res.data.response[0].produk?.map((d) => d)
                  ).map((d) => d),
                  borderColor: "#5C69A3",
                },
                {
                  label: "Resep",
                  backgroundColor: "#555C05",
                  data: dataReal(res.data.response[0].resep?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#555C05",
                },
              ],
            };
            this.dataSemuaCabang = {
              labels: this.bulanSelect,
              datasets: [
                {
                  label: "Semua",
                  backgroundColor: "#565454",
                  data: dataReal(res.data.response[0].semua?.map((d) => d)).map(
                    (d) => d
                  ),
                  borderColor: "#565454",
                },
              ],
            };

            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },

    topPrductJasaCabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          `${this.API}//api/1.0/workflow/extrarest/v1/s/topProduk/${
            cookie?.get("user")?.id_cabang
          }/${this.bulan}/${this.tahun}`,
          config
        )
        .then((res) => {
          if (res.data.response) {
            this.topjasa = res.data.response[0].topjasa;
            this.topproduk = res.data.response[0].topproduk;
            this.topresep = res.data.response[0].topresep;
            this.topapotek = res.data.response[0].topapotek;
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },
    topPrductJasaUtama() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          `${this.API}//api/1.0/workflow/extrarest/v1/s/topProduk/${this.cabang}/${this.bulan}/${this.tahun}`,
          config
        )
        .then((res) => {
          if (res.data.response) {
            this.topjasa = res.data.response[0].topjasa;
            this.topproduk = res.data.response[0].topproduk;
            this.topresep = res.data.response[0].topresep;
            this.topapotek = res.data.response[0].topapotek;
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },

    async addakunCabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      function generateToken(length) {
        const chars =
          "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let token = "";

        for (let i = 0; i < length; i++) {
          const randomIndex = Math.floor(Math.random() * chars.length);
          token += chars[randomIndex];
        }

        return md5(token);
      }

      const tokenLength = 20; // You can adjust the length of the token as needed
      const generatedToken = generateToken(tokenLength);

      let varA = [
        {
          NAMA_LENGKAP: `${this.NamaLengkap}`,
          USERNAME: `${this.Uname}`,
          PASSWORD: `${md5(this.Upassowrd)}`,
          ID_JABATAN: `${this.jabatan}`,
          ID_CABANG: `${this.cabang}`,
          TOKEN: `${generatedToken}`,
          STATUS: `1`,
          NOMOR_INDUK: `${this.noInduk}`,
        },
      ];

      let jsonString = JSON.stringify(varA);
      const obj = { data: jsonString };
      try {
        await axios
          .post(
            `https://api-auth-test-rl4zdrcxca-et.a.run.app/api/1.0/workflow/pmtable/bulk-insert/6596862976103602e5f1c76095811621`,
            obj,
            config
          )
          .then((res) => {
            if (res.status == 200) {
              this.succes = true;

              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          });
      } catch (error) {
        console.log(error);
      }
      this.isLoading = true;
    },

    getListOwner() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      axios
        .get(
          `${this.API}/api/1.0/workflow/extrarest/v1/s/listAkunOwner`,
          config
        )
        .then((res) => {
          if (res.status == 200) {
            this.succes = true;
            this.ListOwner = res.data.response;
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },
    async UpdateOwner(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      const data = {
        ID_KARYAWAN: id,
        USERNAME: this.UsernameOwner,
        PASSWORD: md5(this.PasswordOwner),
      };

      try {
        await axios.put(
          `https://api-auth-test-rl4zdrcxca-et.a.run.app/api/1.0/workflow/pmtable/6596862976103602e5f1c76095811621/data`,
          data,
          config
        );
      } catch (error) {
        console.log(error);
      }
    },

    ListGudang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      axios
        .get(`${this.API}/api/1.0/workflow/extrarest/v1/s/listGudang`, config)
        .then((res) => {
          if (res.status === 200) {
            this.listGudang = res.data.response;
          } else {
            console.log("Error Get List Gudang");
          }
        });
    },

    ViewStokKosong() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;

      axios
        .get(
          `${this.API}/api/1.0/workflow/extrarest/v1/s/listStokKosong/${
            cookie.get("jabatan") == 1
              ? this.cabang
              : cookie?.get("user")?.id_cabang
          }/${this.gudang}`,
          config
        )

        .then((res) => {
          console.log(res);
          if (res.status === 200) {
            this.stokKosong = res.data.response;
            this.isLoading = false;
          } else {
            this.isLoading = false;
          }
        });
    },

    async getAllJumlahTransaksiCabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      try {
        await axios
          .get(
            `${
              this.API
            }//api/1.0/workflow/extrarest/v1/s/pendapatanHarianCabang/${
              cookie?.get("user")?.id_cabang
            }/${this.bulan}/${this.tahun}`,
            config
          )
          .then((res) => {
            if (res.data.response[0]) {
              this.dataJumlahTransaksi = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Apotek",
                    backgroundColor: "#D1AD9F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].apotek?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#D1AD9F",
                  },
                  {
                    label: "Jasa",
                    backgroundColor: "#B74A1F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].jasa?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#B74A1F",
                  },
                  {
                    label: "Produk",
                    backgroundColor: "#5C69A3",
                    data: dataJumlahTransaksi(
                      res.data.response[0].produk?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5C69A3",
                  },
                  {
                    label: "Resep",
                    backgroundColor: "#555C05",
                    data: dataJumlahTransaksi(
                      res.data.response[0].resep?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#555C05",
                  },
                ],
              };
              this.dataJumlahSemuaTransaksi = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Semua",
                    backgroundColor: "#5F5F5F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].semua?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5F5F5F",
                  },
                ],
              };
              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          });
      } catch (error) {
        console.log(error);
      }
    },

    async getAllJumlahTransaksi() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      try {
        await axios
          .get(
            `${this.API}//api/1.0/workflow/extrarest/v1/s/pendapatanHarianCabang/${this.cabang}/${this.bulan}/${this.tahun}`,
            config
          )
          .then((res) => {
            if (res.data.response[0]) {
              this.dataJumlahTransaksi = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Apotek",
                    backgroundColor: "#D1AD9F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].apotek?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#D1AD9F",
                  },
                  {
                    label: "Jasa",
                    backgroundColor: "#B74A1F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].jasa?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#B74A1F",
                  },
                  {
                    label: "Produk",
                    backgroundColor: "#5C69A3",
                    data: dataJumlahTransaksi(
                      res.data.response[0].produk?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5C69A3",
                  },
                  {
                    label: "Resep",
                    backgroundColor: "#555C05",
                    data: dataJumlahTransaksi(
                      res.data.response[0].resep?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#555C05",
                  },
                ],
              };
              this.dataJumlahSemuaTransaksi = {
                labels: this.arrayOfDates,
                datasets: [
                  {
                    label: "Semua",
                    backgroundColor: "#5F5F5F",
                    data: dataJumlahTransaksi(
                      res.data.response[0].semua?.map((d) => d)
                    ).map((d) => d),
                    borderColor: "#5F5F5F",
                  },
                ],
              };
              this.isLoading = false;
            } else {
              this.isLoading = false;
            }
          });
      } catch (error) {
        console.log(error);
      }
    },

    async AnalisaProd() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      this.isLoading = true;
      try {
        const res = await axios.get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/penjualanProdukCabang/${
              this.cabang ? this.cabang : cookie?.get("user")?.id_cabang
            }/${this.bulan}/${this.tahun}/${
              this.tipe === "Semua" ? "all" : this.tipe
            }`,
          config
        );
        if (res.status === 200) {
          this.isLoading = false;
          this.cobac = res.data.response;
          this.totalprice = this.cobac.map((item) => ({
            ...item,
            totalprice: item.hargasatuan * item.totalqty,
          }));
          this.SumAnalisa = this.cobac.reduce(
            (data, item) => data + parseInt(item.totalqty),
            0
          );
          this.Sum = formatRupiah(
            this.totalprice.reduce((data, item) => data + item.totalprice, 0)
          );
        }
      } catch (error) {
        this.isLoading = false;
        console.log(error);
      }
    },
    async AnalisaProdVs() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      this.isLoading = true;
      try {
        const res = await axios.get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/penjualanProdukCabang/${
              this.cabangvs ? this.cabangvs : cookie?.get("user")?.id_cabang
            }/${this.bulanvs}/${this.tahunvs}/${
              this.tipevs === "Semua" ? "all" : this.tipevs
            }`,
          config
        );
        if (res.status === 200) {
          this.isLoading = false;
          this.cobacvs = res.data.response;
          this.totalpricevs = this.cobacvs.map((item) => ({
            ...item,
            totalprice: item.hargasatuan * item.totalqty,
          }));

          this.SumAnalisaVs = this.cobacvs.reduce(
            (data, item) => data + parseInt(item.totalqty),
            0
          );
          this.SumVS = formatRupiah(
            this.totalpricevs.reduce((data, item) => data + item.totalprice, 0)
          );
        }
      } catch (error) {
        this.isLoading = false;
        console.log(error);
      }
    },

    // insentif
    getInsentif() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/insentifTindakanCabang/${
              this.cabang ? this.cabang : cookie.get("idcabang")
            }/${this.jenisTindakan === "Dokter" ? 11 : 21}/${this.bulan}/${
              this.tahun
            }`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.DataTindakan = res.data.response;
        });
    },

    async getInsentifAll() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/insentifTindakanCabang2/${
              this.cabang ? this.cabang : "all"
            }/${this.bulan}/${this.tahun}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.DataTindakan = res.data.response;
        });
    },

    async getInsentifAllCabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
          "Content-Type": "multipart/form-data",
        },
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/insentifTindakanCabang2/${cookie.get(
              "idcabang"
            )}/${this.bulan}/${this.tahun}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.DataTindakan = res.data.response;
        });
    },

    getInsentifApoteker() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      const data = {
        tgl_awal: formattedDate(this.tglAwal),
        tgl_akhir: formattedDate(this.tglAkhir),
        apakek: 0,
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/insentifApoteker/${
              this.cabang ? this.cabang : cookie.get("idcabang")
            }/${data.tgl_awal}/${data.tgl_akhir}/${data.apakek}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.ListApoteker = res.data.response[0].semua_insentif;
        });
    },
    getInsentifDokter() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };

      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/insentifDokter/${
              this.cabang ? this.cabang : cookie.get("idcabang")
            }/${formattedDate(this.tglAwal)}/${formattedDate(this.tglAkhir)}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.HttdDokter = res.data.response[0].httd;
          this.ProdukDokter = res.data.response[0].produk;
          this.ProdukTdkDitebus = res.data.response[0].produk_tdk_ditebus;

          this.ProdukDokter.forEach((item1) => {
            const matchingItem = this.ProdukTdkDitebus.find(
              (item2) => item2.id_user === item1.id_user
            );
            if (matchingItem) {
              this.ProdukmergedArray.push({
                id_user: item1.id_user,
                DokterName: item1.nama_dokter,
                total_ditebus: item1.total_produk,
                total_diresep: matchingItem.total_produk,
                produkTebus: item1.jml_produk,
                produk_tdk_ditebus: matchingItem.jml_produk,
                total_produk: item1.total_produk,
              });
            } else {
              this.ProdukmergedArray.push({
                id_user: item1.id_user,
                DokterName: item1.nama_dokter,
                total_produk: item1.total_produk,
                produkTebus: item1.jml_produk,
                produk_tdk_ditebus: 0,
              });
            }
          });
          console.log(this.ProdukmergedArray);
        });
    },

    getListGudang() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      axios
        .get(this.API + `/api/1.0/workflow/extrarest/v1/s/listGudang`, config)
        .then((res) => {
          this.listGudang = res.data.response;
        });
    },

    getStokProduk() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/lihatStokGudang/${
              this.cabang ? this.cabang : cookie.get("idcabang")
            }/${this.gudang}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.StokProduk = res.data.response;
        });
    },

    getListInventory() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      axios
        .get(this.API + `/api/1.0/workflow/extrarest/v1/s/listJenisInv`, config)
        .then((res) => {
          this.ListInventory = res.data.response;
          this.ListInventory.push({ id: "all", text: "All Inventory" });
        });
    },
    historyproduk() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      const data = {
        tgl_awal: formattedDate(this.tglAwal),
        tgl_akhir: formattedDate(this.tglAkhir),
      };
      this.isLoading = true;
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/laporanInventory/${
              this.cabang ? this.cabang : cookie.get("idcabang")
            }/${data.tgl_awal}}/${data.tgl_akhir}/${this.IdInventory}`,
          config
        )
        .then((res) => {
          this.isLoading = false;
          this.LogInventory = res.data.response;
        });
    },

    getPenjualanVoucher() {
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      axios
        .get(
          this.API + `/api/1.0/workflow/extrarest/v1/s/penjualanVoucherEvent`,
          config
        )
        .then((res) => {
          this.penjualanVoucher = res.data.response;
          console.log(this.penjualanVoucher);
        });
    },
    getpemakaianVoucher() {
      this.pemakaianVoucher = []
      this.isLoading = true;
      const config = {
        headers: {
          Authorization: "Bearer " + cookie.get("token"),
        },
      };
      axios
        .get(
          this.API +
            `/api/1.0/workflow/extrarest/v1/s/pemakaianVoucherCabang/${this.cabang}`,
          config
        )
        .then((res) => {
          this.pemakaianVoucher = res.data.response;
          this.isLoading = false;
          console.log(this.pemakaianVoucher)
        });
    },
  },
});
