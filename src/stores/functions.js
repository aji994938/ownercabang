import moment from "moment";

export function dateNumbers(date) {
  date = new Date(date);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  return [year, month, day].join("-");
}

export function dateFormat(date) {
  date = new Date(date);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  return new Date([year, month, day].join("-"));
}

export function formattedDate(date) {
  return moment(date).format("YYYY-MM-DD");
}
export function formatRupiah(number) {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
  }).format(number);
}



export function dataReal(data) {
  const datadata = Array.from({ length: 12 }, () => "0");
  for (const entry of data) {
    const bulan = entry.bulan;
    if (bulan >= 1 && bulan <= 12) {
      datadata[bulan - 1] = entry.total;
    }
  }
  return datadata;
}

export function dataTgl(data) {
  const datadata = Array.from({ length: 31 }, () => "0");
  for (const entry of data) {
    const day = new Date(entry.tgl).getDate();
    if (day >= 1 && day <= 31) {
      datadata[day - 1] = entry.total;
    }
  }
  return datadata;
}

export function dataJumlahTransaksi(data) {
  const datadata = Array.from({ length: 31 }, () => "0");
  for (const entry of data) {
    const day = new Date(entry.tgl).getDate();
    if (day >= 1 && day <= 31) {
      datadata[day - 1] = entry.qty;
    }
  }
  return datadata;
}
