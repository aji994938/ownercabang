// services/api.js
import axios from 'axios';
import crypto from 'crypto'
// require('dotenv').config();
/**
 * Generate authentication headers based on method and path
 */
function generate_headers(method, pathWithQueryParam) {
    let us = 'jWYkbI0OAdfH1eCA'
    let se = 'm30EGXsk4W81XKE6wYzWf1Ccn8tB66PC'
    let datetime = new Date().toUTCString();
    let requestLine = `${method} ${pathWithQueryParam} HTTP/1.1`;
    let payload = [`date: ${datetime}`, requestLine].join("\n");  
    let signature = crypto
      .createHmac("SHA256", se)
      .update(payload)
      .digest("base64");
  
    return {
      "Content-Type": "application/json",
      Date: datetime,
      Authorization: `hmac username="${us}", algorithm="hmac-sha256", headers="date request-line", signature="${signature}"`,
    };
  }

/**
 * Make an authenticated request using Axios
 */
async function makeAuthenticatedRequest(method, path, queryParam = '', headers = {}) {
    
    let base = 'http://localhost:3000'
   
  let options = {
    method: method,
    url: `${base}${path}${queryParam}`,
    headers: { ...generate_headers(method, path + queryParam), ...headers },
  };
  console.log(options);

  try {
    const response = await axios(options);
   
    return response.data;
  } catch (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);
    } else if (error.request) {
      console.error(error.request);
    } else {
      console.error('Error', error.message);
    }
    throw error;
  }
}

export { makeAuthenticatedRequest };
